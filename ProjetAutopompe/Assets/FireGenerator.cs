﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FireGenerator : MonoBehaviour {

    public static FireGenerator InstanceInTheScene;
    public GameObject [] _prefab;
    public List<GameObject> _createdFires= new List<GameObject>();
    public int _maxFire = 20;

	// Use this for initialization
	void Awake () {
        InstanceInTheScene = this;

    }
    public GameObject GenerateFire(Vector3 position)
    {
        return GenerateFire(position, Quaternion.identity);
    }
    public GameObject GenerateFire (Vector3 position, Quaternion rotation )
    {
        if (_createdFires.Count > _maxFire)
            return null;
        GameObject obj = CreateFire(position, rotation);
        AjoutInFireDataBase(obj);
        return obj;

    }

    private void AjoutInFireDataBase(GameObject obj)
    {
        _createdFires.Add(obj);
        _createdFires= _createdFires.Where(x => x != null).ToList();
    }

    private GameObject CreateFire(Vector3 position, Quaternion rotation)
    {
        return Instantiate(GetRandomFireModel(), position, rotation);
    }

    private GameObject GetRandomFireModel()
    {
        return _prefab[Random.Range(0, _prefab.Length)];
    }
}
