﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisableCanvas : MonoBehaviour {

    public GameObject PanelInGame;
    public GameObject PanelEndGames;
   

	// Use this for initialization
	void Start () {

      


	}
	
	// Update is called once per frame
	void Update () {
        if (TimerScript.IsTime)
        {
          
                PanelEndGames.SetActive(true);
          
                PanelInGame.SetActive(false);
            
        }
       else if (!TimerScript.IsTime)
        {
                PanelInGame.SetActive(true);
        
                PanelEndGames.SetActive(false);
            
        }

   
	}
    
}
