﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class TimerScript : MonoBehaviour {

    public static bool IsTime = false;

    public Text text;
    public int timer = 50;

	// Use this for initialization
	void Start () {
        text.GetComponent<Text>();
        if (timer >= 0)
        {
            InvokeRepeating("TimerChange", 0, 1);
        }
    }



    // Update is called once per frame
    void TimerChange () {
        
        text.text = "Timer: " + timer;
        

        if (timer == 0)
        {
            IsTime = true;
            //TODO.....
        }
        else
        {
            timer--;
            IsTime = false;
        }

    }

 
    
}
