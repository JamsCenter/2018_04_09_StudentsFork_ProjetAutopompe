﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {


    public int score;
    public float repeatRate = 0.5f;
    public Text scoreText;

    private void Start()
    {
        PointWhenDead.SetScoreToZero();
        InvokeRepeating("CheckScore", 0, repeatRate);
    }

    public static ScoreManager instance = null;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    public void CheckScore()
    {
        if(scoreText != null)
        scoreText.text = PointWhenDead.scoreTotal.ToString();
    }
}
