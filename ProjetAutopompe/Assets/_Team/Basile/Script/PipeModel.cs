﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeModel : MonoBehaviour {

    public GameObject model;
    public GameObject[] anchors;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

    public bool IsPipeActive()
    {
        return model.activeInHierarchy;
    }

    public GameObject[] GetAnchors()
    {
        return anchors;
    }
}
