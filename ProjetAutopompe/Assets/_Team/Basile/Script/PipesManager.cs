﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipesManager : MonoBehaviour
{
    public float waterUpdateRate;
    public List<GameObject> pipes = new List<GameObject>();
    public GameObject pipe = new GameObject();


    // Use this for initialization
    void Start()
    { 
         float waterInPipe = pipe.GetComponent<PipeWaterState>().CompletionPercent += 0.001f;
       
    }

    // Update is called once per frame
    void Update()
    {
        WaterStart();
    }

    public void MoveWater(GameObject pipe)
    {
       
    }

    void WaterStart()
    {
        for (int i = 0; i < pipes.Count; i++)
        {
            if (pipes[i].GetComponent<PipeWaterState>().CompletionPercent == 1)
            {
                MoveWater(pipes[i + 1]);
                InvokeRepeating("MoveWater", 0, 0.001f);
            }
            if (pipe.GetComponent<PipeWaterState>().CompletionPercent < 1)
            {
                MoveWater(pipes[i]);
            }
        }

        InvokeRepeating("MoveWater", 0, 0.001f);
    }



}
