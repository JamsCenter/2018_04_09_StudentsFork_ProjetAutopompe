﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayScore : MonoBehaviour {
    public Text finalScore;

	// Use this for initialization
	void Start () {
        finalScore.text = PointWhenDead.scoreTotal.ToString();
	}
}
