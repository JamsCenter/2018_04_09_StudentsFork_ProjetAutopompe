﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateWaterJet : MonoBehaviour {

    public SimpleTouchController rightController;
    public float maxSteeringAngle; // maximum steer angle the wheel can have
    public Vector2 touchposition;
    public float steering;
    //public WaterParticulesSystem _particle;
    public float maxAngleY;
    public float angleY;
    
    private void FixedUpdate()
    {
        touchposition = rightController.GetTouchPosition;
        //jet = _particle._minSpeed * touchposition.y;
        //_particle.SetPourcentPower(jet);
        angleY = maxAngleY * touchposition.y + 25f;
        
        steering = maxSteeringAngle * touchposition.x;// Input.GetAxis("Horizontal");
        transform.localRotation = Quaternion.Euler(-angleY, steering, 0) ;
    }
}
