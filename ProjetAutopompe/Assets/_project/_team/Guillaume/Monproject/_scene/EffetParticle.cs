﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffetParticle : MonoBehaviour {

    public ParticleSystem Particle;
    public float currentValue;
    public float accel = 0f;


  



    // Use this for initialization
    void Start() {
        Particle = GetComponent<ParticleSystem>();

        
    }

    // Update is called once per frame
    void Update() {

        Water();

    }

    void OnParticleCollision(GameObject other)
    {
        float value = Mathf.Lerp(other.transform.localScale.x, 0.1f, Time.deltaTime);
        float valueScale = 0.2f;
        Vector3 vec = new Vector3(valueScale, 0.2f, 0.2f);
        if (other.tag == ("particle"))
        {
            var main = Particle.main;

            var la = main.startSpeed.constant;

            main.startSpeed = 0; 
        }

        if (other.tag == ("Fire"))
        {
            other.transform.localScale = new Vector3(value, value, value);

            if (other.transform.localScale.x <= vec.x)
            {
                print("je suis rentrer");
                Destroy(other.gameObject);
            }
        }

    }

    public void Water()
    {
        float h = Input.GetAxis("Mouse ScrollWheel");
        //Debug.Log(h);
        var main = Particle.main;

        var la = main.startSpeed.constant;
        //main.startSpeed = h;
        //if (Input.GetButtonDown("Fire1"))
        //{
        //    main.Play();
        //}

        if (h > 0)
        {
            
            currentValue = h;
            accel += currentValue;
            main.startSpeed = la += accel;
        }
        else if (h < 0)
        {
            currentValue = h;
            accel -= currentValue;
            main.startSpeed = la -= accel;
        }
    }


}
