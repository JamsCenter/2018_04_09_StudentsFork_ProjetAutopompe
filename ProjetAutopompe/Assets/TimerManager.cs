﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TimerManager : MonoBehaviour {

    public Text timerText;
    public int currentTime = 120;
    public UnityEvent OnGameEnd;
	// Use this for initialization
	void Start () {
        Time.timeScale = 1;
        InvokeRepeating("DecreaseTime", 0, 1);
	}

    void DecreaseTime()
    {
        
        currentTime--;
        if( currentTime <= 0)
        {
            currentTime = 0;
            OnGameEnd.Invoke();
            CancelInvoke();
        }
        timerText.text = currentTime.ToString();
    }

    public void DisplayEndScene()
    {
        SceneManager.LoadScene("MenuLose");
    }
}
