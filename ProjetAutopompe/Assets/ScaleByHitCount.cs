﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ScaleByHitCount : MonoBehaviour {

    public Transform _scale;


    public int _destoyAfter = 50;
    public float _endSize=1;
    public float _startSize;
    public UnityEvent _onDestroy;
    public void Awake()
    {
        _startSize = _scale.localScale.x;
    }
    // Use this for initialization
    public void SetScaleWithCount (int count) {
        float size =(_endSize + (1f - ((float)count / (float)_destoyAfter)) / 2f) * _startSize;
            // Mathf.Clamp(1f - (count * 0.01f),0.1f,1f);
        _scale.localScale = new Vector3(size, size, size);

        if (_destoyAfter < count) {
            _onDestroy.Invoke();
            Destroy(_scale.gameObject);

        }
        
    }
	
}
